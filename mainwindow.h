#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "graphicsview.h"

#include <QMainWindow>
#include <ui_mainwindow.h>


namespace Ui { class MainWindow; }


class MainWindow : public QMainWindow
{
    Q_OBJECT


public:

    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
//    GraphicsView *view;
    Ui::MainWindow *ui;
    QGraphicsScene *scene;
    Node* circle;
    QString path;

private slots:
    void on_saveButton_clicked();

};
#endif // MAINWINDOW_H
