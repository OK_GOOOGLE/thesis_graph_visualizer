#include "mainwindow.h"
#include "Node.h"
#include "graphicsview.h"
#include <QApplication>
#include <QGraphicsScene>
#include <QtWidgets>

using namespace std;


int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    MainWindow w;
    w.show();
    return app.exec();
}
