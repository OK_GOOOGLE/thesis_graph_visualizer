#include <QSvgGenerator>
#include <cmath>
#include <iostream>
#include "Node.h"
#include "graphicsview.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QFileDialog"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
//    view = new GraphicsView();
//    view->setRenderHint(QPainter::Antialiasing);
//    view->setViewportUpdateMode(QGraphicsView::BoundingRectViewportUpdate);
//    view->setBackgroundBrush(QColor(230, 200, 167));
    this->setWindowTitle("Scribble");
//    this->setCentralWidget(view);
    resize(500,500);


    scene = new QGraphicsScene(-200, -200, 400, 400);
    ui->graphicsView->setScene(scene);
    ui->graphicsView->setBackgroundBrush(QColor(230, 200, 167));
    for (int i = 0; i < 10; ++i) {
        Node *item = new Node;
        QGraphicsLineItem *edge = new QGraphicsLineItem;
        QGraphicsLineItem *edge1 = new QGraphicsLineItem;
        int x = static_cast<int>(::sin((i * 6.28) / 10.0) * 150);
        int y = static_cast<int>(::cos((i * 6.28) / 10.0) * 150);
        int xEnd = static_cast<int>(::sin(((i+1)%10 * 6.28) / 10.0) * 150);
        int yEnd = static_cast<int>(::cos(((i+1)%10 * 6.28) / 10.0) * 150);

//        edge->setP1(QPoint(x,y));
//        edge->setP1(QPoint(xEnd,yEnd));
        edge->setLine(x, y, xEnd, yEnd);
        edge1->setLine(x, y, 0, 0);
        item->setPos(x, y);
        scene->addItem(item);
        scene->addItem(edge);
        scene->addItem(edge1);
//        std::cout << "item added()\n";
    }
    circle = new Node;
    circle->setPos(5, 5);
    scene->addItem(circle);
//    this->setScene(scene);
}

MainWindow::~MainWindow(){
    delete ui;
}


void MainWindow::on_saveButton_clicked(){
    QString newPath = QFileDialog::getSaveFileName(this, "Save SVG",
            path, tr("SVG files (*.svg)"));

        if (newPath.isEmpty())
            return;

        path = newPath;
        int h = static_cast<int>(scene->height());
        int w = static_cast<int>(scene->width());
        QSvgGenerator generator;
        generator.setFileName(path);
        generator.setSize(QSize(w, h));
        generator.setViewBox(QRect(0, 0, w, h));
        generator.setTitle("SVG Example");
        generator.setDescription("File created by SVG Example");

        QPainter painter;
        painter.begin(&generator);  // set object in which drawing will take place
        scene->render(&painter);
        painter.end();
}

