#ifndef DRAGABLECIRCLE_H
#define DRAGABLECIRCLE_H

class Edge;

#include <QGraphicsItem>

class Node : public QGraphicsItem
{
public:
    Node();

    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
    QPainterPath shape() const override; // bounding shape to be ellipse instead of rectangle

    void addEdge(Edge *edge);

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;

private:
    QVector<Edge *> edgeList;
//    QLine edge;
    QPointF offset;
    QPointF dragStart;
    QColor color;
};

#endif // DRAGABLECIRCLE_H
