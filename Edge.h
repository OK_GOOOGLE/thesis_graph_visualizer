#ifndef EDGE_H
#define EDGE_H


#include <QGraphicsItem>


class Node;
class Edge : public QGraphicsItem
{
public:
    Edge();

    Node *sourceNode() const;
    Node *destNode() const;
    void adjust();


private:
    Node *source, *dest;

    QPointF sourcePoint;
    QPointF destPoint;
};

#endif // EDGE_H
