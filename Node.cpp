#include <QRect>
#include <QtWidgets>
#include "Node.h"
#include "Edge.h"
#include <iostream>

Node::Node()
    : color(QRandomGenerator::global()->bounded(256),
            QRandomGenerator::global()->bounded(256),
            QRandomGenerator::global()->bounded(256))
{
    setCursor(Qt::OpenHandCursor);
    setFlag(ItemIsMovable);
    setFlag(ItemSendsGeometryChanges);
    setAcceptedMouseButtons(Qt::LeftButton);
    setCacheMode(DeviceCoordinateCache);
    setZValue(100); // nodes over edges // TODO
}

QRectF Node::boundingRect() const{
    return QRectF(-15, -15, 30, 30);
}

void Node::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option)
    Q_UNUSED(widget)

    painter->setPen(QPen(Qt::black, 1));
    painter->setBrush(QBrush(color));
//    painter->drawRect(-10, -10, 30, 30);
    painter->drawEllipse(-15, -15, 30, 30);
}

void Node::mousePressEvent(QGraphicsSceneMouseEvent *event){
    setCursor(Qt::ClosedHandCursor);
}

void Node::mouseReleaseEvent(QGraphicsSceneMouseEvent * event){
    setCursor(Qt::ArrowCursor);
}

void Node::mouseMoveEvent(QGraphicsSceneMouseEvent *event){
    this->setPos(mapToScene(event->pos()));
}

QPainterPath Node::shape() const{
    QPainterPath path;
    path.addEllipse(-15, -15, 30, 30);
    return path;
}

void Node::addEdge(Edge *edge){
    edgeList << edge;
    edge->adjust(); // The edge is adjusted so that the end points for the edge match the positions of the source and destination nodes.
}

/*
DragableCircle::DragableCircle()
    : color(QRandomGenerator::global()->bounded(256), QRandomGenerator::global()->bounded(256), QRandomGenerator::global()->bounded(256))
{
//    setToolTip(QString("QColor(%1, %2, %3)\n%4")
//              .arg(color.red()).arg(color.green()).arg(color.blue())
//              .arg("Click and drag this circle"));
//    ensures that the cursor will change to OpenHandCursor when the mouse pointer hovers over the item
    setCursor(Qt::OpenHandCursor);
//    to ensure that this item can only process LeftButton
    setAcceptedMouseButtons(Qt::LeftButton);
}

QRectF DragableCircle::boundingRect() const
{
    return QRectF(-15.5, -15.5, 34, 34);
}

//draws an ellipse with a 1-unit black outline, a plain color fill, and a dark gray dropshadow.
void DragableCircle::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option)
    Q_UNUSED(widget)
    painter->setPen(QPen(Qt::black, 1));
    painter->setBrush(QBrush(color));
    painter->drawEllipse(-15, -15, 30, 30);
}


//handler is called when the mouse button pressed inside the item's area
void DragableCircle::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
//    setCursor(Qt::ClosedHandCursor);
    offset = event->pos();
    dragStart = event->pos();
}

//called when the mouse button released after having pressed it inside an item's area
void DragableCircle::mouseReleaseEvent(QGraphicsSceneMouseEvent * event)
{

    setCursor(Qt::OpenHandCursor);
    QPointF point = event->buttonDownPos(Qt::LeftButton);
    std::cout << "released\n";
    std::cout << point.x() << ", " << point.y() << "\n";
//    QPointF position = event->scenePos();
//    offset = event->pos();
//    this->setPos(position);
//    this->moveBy(offset.x(), offset.y());

}


void DragableCircle::mouseMoveEvent(QGraphicsSceneMouseEvent *event){
//    to eliminate mouse noise
    if (QLineF(event->screenPos(), event->buttonDownScreenPos(Qt::LeftButton)).length() < QApplication::startDragDistance()) {
        return;
    }    

    offset = event->pos();
    QPointF position = (event->scenePos());
//    std::printf("%.3f, %.3f\n", position.x(), position.y());


    QDrag *drag = new QDrag(event->widget());
    QMimeData *mime = new QMimeData;
    drag->setMimeData(mime);
    mime->setColorData(color);
    QPixmap pixmap(34, 34);
    pixmap.fill(Qt::white);

    QPainter painter(&pixmap);
    painter.translate(15, 15);
    painter.setRenderHint(QPainter::Antialiasing);
    paint(&painter, 0, 0);
    painter.drawLine(position + QPointF(20, 20), position);
    painter.end();

    pixmap.setMask(pixmap.createHeuristicMask()); // removes white box while dragging

    drag->setPixmap(pixmap);
    drag->setHotSpot(QPoint(15, 20));
    this->setPos(position);
    drag->exec();
    setCursor(Qt::OpenHandCursor);

}

*/
