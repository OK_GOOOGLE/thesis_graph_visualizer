#include "graphicsview.h"
#include <cmath>
#include <QResizeEvent>
#include <iostream>
#include <QDrag>
#include <QMimeData>
#include <qapplication.h>
GraphicsView::GraphicsView(){
    scene = new QGraphicsScene(-200, -200, 400, 400);
    for (int i = 0; i < 10; ++i) {
        Node *item = new Node;
        item->setPos(::sin((i * 6.28) / 10.0) * 150,
                     ::cos((i * 6.28) / 10.0) * 150);
        scene->addItem(item);
//        std::cout << "item added()\n";
    }
    circle = new Node;
    circle->setPos(5, 5);
    scene->addItem(circle);
    this->setScene(scene);
}

GraphicsView::GraphicsView(QWidget *widget) : QGraphicsView(widget){
}

void GraphicsView::resizeEvent(QResizeEvent *event){
}
