#ifndef GRAPHICSVIEW_H
#define GRAPHICSVIEW_H

#include "Node.h"
#include <QGraphicsView>
#include <QObject>

class GraphicsView : public QGraphicsView{
    Node *circle;
    QGraphicsScene *scene;
    QPointF offset;
    QPointF dragStart;
public:
    GraphicsView();
    GraphicsView(QWidget *widget);
//    GraphicsView(QGraphicsScene *scene);
//    void mouseReleaseEvent(QMouseEvent *event) override;
//    void mousePressEvent(QMouseEvent *event) override;


protected:
    void resizeEvent(QResizeEvent *event) override;
};

#endif // GRAPHICSVIEW_H
